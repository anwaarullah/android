package com.andtronics.user;

public class ValueMsg {

	private String message;
	private int value;
	
	public ValueMsg(int val) {
		this.value= val;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
}
