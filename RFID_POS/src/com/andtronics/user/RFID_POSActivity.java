package com.andtronics.user;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.android.future.usb.UsbAccessory;
import com.android.future.usb.UsbManager;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.widget.TextView;

public class RFID_POSActivity extends Activity implements Runnable {

	TextView input;
	private UsbAccessory mAccessory;
	private FileInputStream mInputStream;
	private FileOutputStream mOutputStream;
	private UsbManager mUSBManager;
	private boolean mPermissionRequestPending;
	private PendingIntent mPermissionIntent;
	private static final String ACTION_USB_PERMISSION = "com.google.android.DemoKit.action.USB_PERMISSION";
	private ParcelFileDescriptor mFileDescriptor;
	private StringBuffer sbu;
	private String str;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		input = (TextView) findViewById(R.id.arduinoinput);
		setupAccessory();
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		if(mAccessory != null)
			return mAccessory;
		else
			return super.onRetainNonConfigurationInstance();
	};

	@Override
	protected void onResume() {
		super.onResume();

		if(mInputStream != null && mOutputStream != null) {
			return;
		}

		UsbAccessory[] accessories = mUSBManager.getAccessoryList();
		UsbAccessory accessory = (accessories == null ? null : accessories[0]);

		if(accessory != null) {
			if(mUSBManager.hasPermission(accessory)) {
				openAccessory(accessory);
			}
			else {
				synchronized (mUSBReceiver) {
					if(!mPermissionRequestPending) {
						mUSBManager.requestPermission(accessory, mPermissionIntent);
						mPermissionRequestPending = true;
					}
				}
			}
		}
		else { }

	};

	@Override
	protected void onDestroy() {
		unregisterReceiver(mUSBReceiver);
		super.onDestroy();
	};

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	};

	Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			ValueMsg t = (ValueMsg) msg.obj;
			input.setText(t.getValue());
		}
	};

	private void openAccessory(UsbAccessory accessory) {

		mFileDescriptor = mUSBManager.openAccessory(accessory);
		if(mFileDescriptor != null) {
			mAccessory = accessory;
			FileDescriptor fd = mFileDescriptor.getFileDescriptor();
			mInputStream = new FileInputStream(fd);
			mOutputStream = new FileOutputStream(fd);
			Thread thread = new Thread(null, this, "RFID_POS");
			thread.start();
			Log.v("ADK", "Open Accessory Connected");
			
		}
		else {	
			Log.v("ADK", "Open Accessory Failed Connection");
		}
	}

	private void setupAccessory() {
		mUSBManager = UsbManager.getInstance(this);
		mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
		IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
		filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
		registerReceiver(mUSBReceiver, filter);
		if(getLastNonConfigurationInstance() != null) {
			mAccessory = (UsbAccessory) getLastNonConfigurationInstance();
			openAccessory(mAccessory);
		}
	}

	public void closeAccessory() {
		try {
			if(mFileDescriptor != null)
				mFileDescriptor.close();
		}
		catch (IOException e) {		}
		finally {
			mFileDescriptor = null;
			mAccessory = null;
		}
	}

	@Override
	public void run() {
		int ret = 0;
		byte[] buffer = new byte[16384];
		int i;
		
		while(true) {
			try {
				ret = mInputStream.read(buffer);
			}
			catch (IOException e) {
				break;
			}
			
			i = 0;
			while(i < ret) {
				int len = ret -i;
				if(len >= 1) {
					Message m = Message.obtain(mHandler);
					int value = (int) buffer[i];
					m.obj = new ValueMsg(value);
					mHandler.sendMessage(m);
				}
				i += 1;
			}
		}
	}

	private final BroadcastReceiver mUSBReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			String action = intent.getAction();
			if(ACTION_USB_PERMISSION.equals(action)) {
				synchronized (getApplicationContext()) {
					UsbAccessory accessory = UsbManager.getAccessory(intent);
					if(intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
						openAccessory(accessory);
					}
					else { }
				}
			}
			else if(UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) {
				UsbAccessory accessory = UsbManager.getAccessory(intent);
				if(accessory != null && accessory.equals(mAccessory))
					closeAccessory();
			}
		}
	}; 
}