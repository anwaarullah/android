package com.sms;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;

public class SMSManagerActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage("9052210433", null, "Android Workshop", null, null);
    }
}