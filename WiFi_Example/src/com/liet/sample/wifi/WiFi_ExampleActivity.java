package com.liet.sample.wifi;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import com.whitebyte.wifihotspotutils.ClientScanResult;
import com.whitebyte.wifihotspotutils.WifiApManager;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class WiFi_ExampleActivity extends Activity {
	WifiApManager wifiApManager;
	InetAddress in;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        wifiApManager = new WifiApManager(getApplicationContext());
        
        	List<ClientScanResult> clients = wifiApManager.getClientList(true, 500);
        	for(ClientScanResult client : clients)
        	{
        		Toast.makeText(getApplicationContext(), "Found Client: " + client.getHWAddr()
        				+ " -- " + client.getDevice() + " -- " + client.getIpAddr(), Toast.LENGTH_LONG).show();
        	}
        }
    }
