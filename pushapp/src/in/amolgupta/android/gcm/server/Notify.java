package in.amolgupta.android.gcm.server;

import java.util.ArrayList;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;

public class Notify {
	public static void main(String args[]) {

		try {

			Sender sender = new Sender(
					"AIzaSyByhn8tW-b96E762N-jFqqXZrTNJQvzTLA");

			ArrayList<String> devicesList = new ArrayList<String>();
			devicesList.add("APA91bEX5NvXkKPczLTdD4QGLhXSxwEiHv13FsiZyV4MyDY4tVl36pLR-UX-aN222gx44MLDQiVSeWpN_Cm2ifpPE4UAEMmVmHTjXhQ13pC9x7q6MOQMYiUdUcpTR8XP_SxZrsC9EUb9RSxbCrHHojGLGNcBcl7vtw");
//			devicesList.add("APA91bH1cc67ghqwUcXyJieu1DLZ3AJ5L_dkNztzlT61I1KucnJiabzKEiOtE72NJAxK6cdSdAMUUmSMP8nhhn73usxITJZ-H49y_HO4ri67YlZqcwXYqW1-2LyQDl0rwfzF_eepwx7we8h9N8QgLfioDqH6R1xlaw");
			
			// Use this line to send message without payload data
			// Message message = new Message.Builder().build();

			// use this line to send message with payload data
			Message message = new Message.Builder()
					.collapseKey("1")
					.timeToLive(3)
					.delayWhileIdle(true)
					.addData("Message",
							"This text will be seen in notification bar!!")
					.build();

			// Use this code to send to a single device
			// Result result = sender
			// .send(message,
			// "APA91bGiRaramjyohc2lKjAgFGpzBwtEmI8tJC30O89C2b3IjP1CuMeU1h9LMjKhmWuZwcXZjy1eqC4cE0tWBNt61Kx_SuMF6awzIt8WNq_4AfwflaVPHQ0wYHG_UX3snjp_U-5kJkmysdRlN6T8xChB1n3DtIq98w",
			// 1);

			// Use this for multicast messages
			MulticastResult result = sender.send(message, devicesList, 1);
			sender.send(message, devicesList, 1);

			System.out.println(result.toString());
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {
				}
			} else {
				int error = result.getFailure();
				System.out.println(error);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
