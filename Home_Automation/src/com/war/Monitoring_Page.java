package com.war;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Monitoring_Page extends Activity{

	private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	private PowerManager.WakeLock wl;
	private static final int REQUEST_DISCOVERY = 0x1;;
	private Handler handler = new Handler();
	private BluetoothSocket socket = null;
	private String str;
	private OutputStream outputStream;
	private InputStream inputStream;
	private StringBuffer sbu;
	Button establishConnection;
	TextView input;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.monitoring_page);
		Button enableBluetooth = (Button) findViewById(R.id.enableBluetooth);
		establishConnection = (Button) findViewById(R.id.establishConnection);
		input = (TextView) findViewById(R.id.input);
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");


		enableBluetooth.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				if(!bluetoothAdapter.isEnabled()) {
					Intent switchOnBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
					startActivity(switchOnBluetooth);
				}
				else if(bluetoothAdapter.isEnabled()) {
				}
			}
		});

		establishConnection.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(), DiscoveryActivity.class);				
				startActivityForResult(intent, REQUEST_DISCOVERY);
				}
		});
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode != REQUEST_DISCOVERY) {
//			finish();
			Toast.makeText(getApplicationContext(), "Couldn't connect to device. Please try again!!!", Toast.LENGTH_LONG).show();
			return;
		}
		if (resultCode != RESULT_OK) {
//			finish();
			Toast.makeText(getApplicationContext(), "Couldn't connect to device. Please try again!!!", Toast.LENGTH_LONG).show();
			return;
		}
		establishConnection.setText("Connected to Device");
		final BluetoothDevice device = data.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		new Thread() {
			public void run() {
				connect(device);
			};
		}.start();
	}

	protected void connect(BluetoothDevice device) {
		//BluetoothSocket socket = null;
		try {
			//Create a Socket connection: need the server's UUID number of registered
			socket = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));

			socket.connect();

			inputStream = socket.getInputStream();														
			outputStream = socket.getOutputStream();
			int read = -1;
			final byte[] bytes = new byte[20480];
			for (; (read = inputStream.read(bytes)) > -1;) {
				final int count = read;
				handler.post(new Runnable() {
					public void run() {

						StringBuilder b = new StringBuilder();
						for (int i = 0; i < count; ++i) {
							String s = Integer.toString(bytes[i]);
							b.append(s);
							b.append(",");
						}
						String s = b.toString();
						String[] chars = s.split(",");
						sbu = new StringBuffer();  
						for (int i = 0; i < chars.length; i++) {  
							sbu.append((char) Integer.parseInt(chars[i]));  
						}
						if(str != null)
						{	
							str += (sbu.toString());
							input.append(str);
						}
						else
						{
							str = sbu.toString();
							input.append(str);
						}
					}
				}); 
			}
		} catch (IOException e) {
			finish();
			return ;
		} finally {
			if (socket != null) {
				try {
					socket.close();	
					finish();
					return ;
				} catch (IOException e) {
				}
			}
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}
	@Override
	protected void onPause() {
		super.onPause();
		wl.release();
	};

	@Override
	protected void onResume() {
		super.onResume();
		wl.acquire();
	};
	protected void writeCharToDevice(String str) {
		String tmpStr = str;
		byte bytes[] = tmpStr.getBytes();
		try {
			outputStream.write(bytes);
		} catch (IOException e) {	}
	}
}
