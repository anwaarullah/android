/* BlueScipts
 * A simple Android program for embedded system communications
 * More information at teholabs.com/docs
 * 
 * Copyright (C) 2011 B. A. Bryce/tehoLabs (Apache Version 2.0 as below)
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.teholabs.bluescripts;

import java.io.File;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * This is the main Activity that displays the current session.
 */
public class BlueScripts extends Activity {
    // Debugging
    private static final String TAG = "BlueScripts";
    private static final boolean D = false;

    // Message types sent from the BluetoothMessageService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothMessageService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;

    // Layout Views
    private TextView mTitle;
    private ListView mConversationView;
    private ListView mScriptsView;

    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    private ArrayAdapter<String> mLogArrayAdapter;    
    private ArrayAdapter<String> mScriptsArrayAdapter;

    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the message services
    private BluetoothMessageService mMessageService = null;    
    
    //some extra vars
    //private BluetoothDevice device = null;    
    private Vector <ScriptObject> myScriptVector = new Vector <ScriptObject>();
    private String replyBuffer = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(D) Log.e(TAG, "+++ ON CREATE +++");

        // Set up the window layout
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.main);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

        // Set up the custom title
        mTitle = (TextView) findViewById(R.id.title_left_text);
        mTitle.setText(R.string.app_name);
        mTitle = (TextView) findViewById(R.id.title_right_text);

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        
        // ************
        //Read in the XML script file and create the script objects
        // ************
        
        // Check that we have mounted media before starting
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED_READ_ONLY.equals(state) || Environment.MEDIA_MOUNTED.equals(state)){
        	//Okay the SD card is there...
        	try{
    			//We need the root path first
    	        File path = Environment.getExternalStorageDirectory();
    	        //Make the file object that references our script xml file
    		    File scriptsXmlFile = new File(path, "BlueScripts.xml");
    		    
    		    //Set up the DOM XML parser
    		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    		    Document doc = dBuilder.parse(scriptsXmlFile);
    		    doc.getDocumentElement().normalize();
    		    
    		    //Get the nodes for the script tag
    		    NodeList scriptList = doc.getElementsByTagName("script");
    		    
    		    
    		    //For each script make a script object and put it in the vector
    		    for (int i = 0; i < scriptList.getLength(); i++) {
    		    	 
    		        Node nNode = scriptList.item(i);	    
    		        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
    		  
    		           Element eElement = (Element) nNode;
    		           myScriptVector.addElement(new ScriptObject(getTagValue("name",eElement)));
    		           NodeList actionList = eElement.getElementsByTagName("action");   
   				    
   				    //Now go through and create the actions the script does
   				    for (int k = 0; k < actionList.getLength(); k++) {
   				    	 
   				        Node nNode2 = actionList.item(k);	    
   				        if (nNode2.getNodeType() == Node.ELEMENT_NODE) {
   				  
   				           Element eElement2 = (Element) nNode2;
			        
   				           myScriptVector.get(i).addAction(getTagValue("address",eElement2), getTagValue("message",eElement2));
   				           
   				         }
   				     }	     
    		           

    		         }
    		     }
    		    
    		} catch(Exception e){
    			Toast.makeText(this, "Could not read/open script XML file", Toast.LENGTH_LONG).show();
    		}
        }
        else{
        	Toast.makeText(this, "Media not readable", Toast.LENGTH_LONG).show();
        }
        
        
    }

    @Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupService() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        // Otherwise, setup the service session
        } else {
            if (mMessageService == null) setupService();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mMessageService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mMessageService.getState() == BluetoothMessageService.STATE_NONE) {
              // Start the Bluetooth message services
              mMessageService.start();
            }
        }
    }

    private void setupService() {
        Log.d(TAG, "setupService()");

        // Initialize the array adapter for the conversation thread
        mLogArrayAdapter = new ArrayAdapter<String>(this, R.layout.message);
        mConversationView = (ListView) findViewById(R.id.in);
        mConversationView.setAdapter(mLogArrayAdapter);
        
        mScriptsArrayAdapter = new ArrayAdapter<String>(this, R.layout.script);
        mScriptsView = (ListView) findViewById(R.id.scripts);
        mScriptsView.setAdapter(mScriptsArrayAdapter);
        
        //Set through the script vector and add all the scripts to our list
        for (int i = 0; i < myScriptVector.size(); i++){
        	mScriptsArrayAdapter.add(myScriptVector.get(i).getName());
        }
        
        // Initialize the BluetoothMessageService to perform bluetooth connections
        mMessageService = new BluetoothMessageService(this, mHandler);
 
        
        mScriptsView.setOnItemClickListener(new OnItemClickListener() {
	    public void onItemClick(AdapterView<?> parent, View view,
	        int position, long id) {
	    	
	    	//Get the script number and run the script
	    	int myIndex = (int)id;	
	    	//
	    	int actionCount = 0;
	    	//Get the first action
	    	ScriptAction tempAction = myScriptVector.get(myIndex).getAction(actionCount);
	    	while(tempAction != null){
	    		replyBuffer = "";
	    		runAction(tempAction); 
	    		actionCount++;
	    		tempAction = myScriptVector.get(myIndex).getAction(actionCount);
	    	}	    	   

		  
	      // When clicked, show a toast with the TextView text
	      Toast.makeText(getApplicationContext(), ((TextView) view).getText(),
	          Toast.LENGTH_SHORT).show();      

	      	      
	    }
	  });
        

        
        

   }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if(D) Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if(D) Log.e(TAG, "-- ON STOP --");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth script services
        if (mMessageService != null) mMessageService.stop();
        if(D) Log.e(TAG, "--- ON DESTROY ---");
    }

    // The Handler that gets information back from the BluetoothMessageService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothMessageService.STATE_CONNECTED:
                    mTitle.setText(R.string.title_connected_to);
                    mTitle.append(mConnectedDeviceName);
                    mLogArrayAdapter.clear();
                    break;
                case BluetoothMessageService.STATE_CONNECTING:
                    mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothMessageService.STATE_NONE:
                    mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_WRITE:
                byte[] writeBuf = (byte[]) msg.obj;
                // construct a string from the buffer
                String writeMessage = new String(writeBuf);
                mLogArrayAdapter.add("Sent:  " + writeMessage);
                break;
            case MESSAGE_READ:
            	// This expects a reply terminated with EOT char 0x04 in ASCII
            	// It will add the reply to the listView EOT is detected
                byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
                if(readBuf[msg.arg1-1] == 0x04){
                    String readMessage = new String(readBuf, 0, msg.arg1-1);
                    replyBuffer += readMessage;   
                	mLogArrayAdapter.add("Reply:  " + replyBuffer);
                }
                else{
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    replyBuffer += readMessage;   
                }
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "
                               + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                               Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
        case REQUEST_CONNECT_DEVICE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                // Get the device MAC address
                String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                // Get the BLuetoothDevice object
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                // Attempt to connect to the device
                mMessageService.connect(device);
            }
            break;
        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth is now enabled, so set up a service session
                setupService();
            } else {
                // User did not enable Bluetooth or an error occured
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.scan:
            // Launch the DeviceListActivity to see devices and do scan
            Intent serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            return true;
        case R.id.clearlog:
            // Clear the second listview
        	mLogArrayAdapter.clear();
            return true;
        }
        return false;
    }
    
    
	public boolean runAction(ScriptAction myAction){
		String mCurrentAddress = myAction.getAddress();
			
		//Set timeout of 5 seconds to connect to the right bluetooth object
		//could add timeout property to ScriptAction object in future rather than fixed value
		long endTimeMillis = System.currentTimeMillis() + 5000;
		
		

		if(mMessageService.getState() != BluetoothMessageService.STATE_CONNECTED || (!mMessageService.getConnectedDevice().getAddress().equals(mCurrentAddress)))
		{
			//Stop any threads	    
	    	if(mMessageService.getState() != BluetoothMessageService.STATE_NONE)mMessageService.stop();
	    	//Set the MAC address
	    	//device = mBluetoothAdapter.getRemoteDevice(mCurrentAddress);	    	
			// Attempt to connect to the device and do the communication
			mMessageService.connect(mBluetoothAdapter.getRemoteDevice(mCurrentAddress)); 
			// Wait for connection
			while(mMessageService.getState() != BluetoothMessageService.STATE_CONNECTED && System.currentTimeMillis() < endTimeMillis){		    	
		    	// do nothing just wait
		    }
		}	    
		//This should be true if it did not timeout
	    if(mMessageService.getState() == BluetoothMessageService.STATE_CONNECTED && mMessageService.getConnectedDevice().getAddress().equals(mCurrentAddress)){
  	    	byte[] send = (myAction.getMessage()+ "\r\n").getBytes();
      	    mMessageService.write(send);
      	    return true;//success
	    }
	    else{
		      Toast.makeText(getApplicationContext(), "Timeout, no connection to target", Toast.LENGTH_SHORT).show();		  
	    }
	    return false;

	}
    
    
    private static String getTagValue(String sTag, Element eElement){
        NodeList nlList= eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = (Node) nlList.item(0); 
     
        return nValue.getNodeValue();    
     }

}