/*
 * Copyright (C) 2011 B. A. Bryce/tehoLabs (Apache Version 2.0 as below)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.teholabs.bluescripts;

public class ScriptAction {
	private String message = null;
	private String address = null;
	
	public ScriptAction(String aAddress, String aMessage){
		address = aAddress;
		message = aMessage;
	}	
	
	public void setAddress(String a){
		address = a;
	}
	
	public void setMessage(String a){
		message = a;
	}
	
	public String getAddress(){
		return address;
	}
	
	public String getMessage(){
		return message;
	}
}
