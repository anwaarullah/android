/*
 * Copyright (C) 2011 B. A. Bryce/tehoLabs (Apache Version 2.0 as below)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.teholabs.bluescripts;

import java.util.Vector;

public class ScriptObject{
	private String name = null;
	private Vector <ScriptAction> actionVector = new Vector <ScriptAction>();	
	
	public ScriptObject(String aName, String aMessage){
		name = aName;
		actionVector.addElement(new ScriptAction("0A:02:08:0F:28:37", aMessage));
	}
	public ScriptObject(String aName){
		name = aName;
	}	
	
	public void setName(String a){
		name = a;
	}	
	
	public String getName(){
		return name;
	}
	
	public ScriptAction getAction(int a){
		if(a<actionVector.size())return actionVector.get(a);
		else return null;
	}
	
	public void addAction(String aAddress, String aMessage){
		actionVector.addElement(new ScriptAction(aAddress, aMessage));
	}
		
}
