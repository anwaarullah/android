package com.home.general;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class ArduinoWiFiActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    	WifiApManager manager = new WifiApManager(getApplicationContext());
        String TAG = "Arduino";
        InetAddress in;
        try {
         List<ClientScanResult> clients = manager.getClientList(true, 500);
         for (ClientScanResult client : clients) {
          Log.d(TAG, "Found client " + client.getHWAddr() + " " + client.getDevice() + " " + client.getIpAddr());
         }

         in = InetAddress.getByName(clients.get(0).getIpAddr());
         if (in.isReachable(500)) {
          Log.d(TAG, "Arduino reachable");
          Socket socket = new Socket(in, 2000);
          for (int i = 0; i < 7; i++) {
           // Read *HELLO*
           socket.getInputStream().read();
          }
         } else {
          Log.d(TAG, "Arduino not reachable");
         }
        } catch (UnknownHostException e) {
         Log.e(TAG, "Could not connect: " + e.getMessage(), e);
        } catch (IOException e) {
         Log.e(TAG, "Could not connect: " + e.getMessage(), e);
        }
    }
}