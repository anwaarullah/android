package com.andtronics.com;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import at.abraxas.amarino.Amarino;

public class Arduino_RobotActivity extends Activity {
	
	private BluetoothDevice targetDevice;
	private boolean deviceFound;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		final Button redButton = (Button) findViewById(R.id.red);
		final Button blueButton = (Button) findViewById(R.id.blue);
		final Button whiteButton = (Button) findViewById(R.id.white);
		final Button bluetoothButton = (Button) findViewById(R.id.bluetooth);
		final Button showBluetoothDeviceButton = (Button) findViewById(R.id.showDevice);
		final Button connectToDeviceButton = (Button) findViewById(R.id.connectToDevice);
		final Toast toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
		final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		
		redButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				toast.setText("Red Clicked");
				toast.show();
				Amarino.sendDataToArduino(getApplicationContext(), getTargetDevice().getAddress(), 'R', "R");
			}
		});

		blueButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				toast.setText("Blue Clicked");
				toast.show();
			}
		}); 

		whiteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				toast.setText("White Clicked");
				toast.show();
			}
		});
		final BroadcastReceiver mReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				if(BluetoothDevice.ACTION_FOUND.equals(action)) {
					BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setText("Device Name: " + device.getName() + "Device Address: " + device.getAddress());
					setTargetDevice(device);
				}
			}
		};
		registerReceiver(mReceiver, filter);

		bluetoothButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(!bluetoothAdapter.isEnabled()) {
					Intent makeDeviceDiscoverable = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
					startActivity(makeDeviceDiscoverable);
				}
				else if(bluetoothAdapter.isEnabled()){
					toast.setText("Bluetooth Already Enabled!!!");
					toast.show();
					setDeviceFound(bluetoothAdapter.startDiscovery());
				}
			}
		});
		
		showBluetoothDeviceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(isDeviceFound()) {
					Toast.makeText(getApplicationContext(), "Device Name: " + getTargetDevice().getName() + "Device Address: " + getTargetDevice().getAddress(), Toast.LENGTH_LONG).show();
				}
			}
		});
		
		connectToDeviceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Amarino.connect(getApplicationContext(), "00:19:A4:02:C6:14");
				Toast.makeText(getApplicationContext(), "BLuetooth Connection established with: RoboLink", Toast.LENGTH_LONG).show();
			}
		});
	}

	public void setTargetDevice(BluetoothDevice targetDevice) {
		this.targetDevice = targetDevice;
	}

	public BluetoothDevice getTargetDevice() {
		return targetDevice;
	}

	public void setDeviceFound(boolean deviceFound) {
		this.deviceFound = deviceFound;
	}

	public boolean isDeviceFound() {
		return deviceFound;
	}
}