package com.liet.user;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;

public class LIET_SMS_ExampleActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage("8686928786", null, "This is the body of the sms.", null, null);
    }
}