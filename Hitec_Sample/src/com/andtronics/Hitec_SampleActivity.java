package com.andtronics;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Hitec_SampleActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        final Toast toast = Toast.makeText(getBaseContext(), "Welcome to Android Programming", Toast.LENGTH_SHORT);

        Button button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				toast.show();
				Intent intent = new Intent(view.getContext(), Second.class);
				startActivity(intent);
			}
		});        
    }
}