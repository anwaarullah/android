package com.war;

import com.war.DBOperation;
import com.war.SampleDatabase;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class WarActivity extends Activity {
	public static SampleDatabase database;
	DBOperation dbOperation;
	int wrongAttempts = 0;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

    
		database = new SampleDatabase(getApplicationContext(), "wartable.db", null, 1);
		database.checkDatabase();
		dbOperation = new DBOperation();
		
		final TextView Username = (EditText) findViewById(R.id.usernameText);
		final TextView Password = (EditText) findViewById(R.id.passwordText);
		Button sButton = (Button) findViewById(R.id.Sbutton);
		sButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if((Username.getText().toString().length()==0) || (Password.getText().toString().length()==0))
				{
					Toast.makeText(getApplicationContext(),"Please enter the Username/Password!",Toast.LENGTH_LONG).show();
				}
				/*				else if(Username.getText().toString().length()==0)
				{
					Toast.makeText(getApplicationContext(),"Please enter the Username!",Toast.LENGTH_LONG).show();
					Username.setText("");
					Password.setText("");
					return;
				}
				else if(Password.getText().toString().length()==0) {
					Toast.makeText(getApplicationContext(), "Please enter the Password!", Toast.LENGTH_LONG).show();
					Username.setText("");
					Password.setText("");
					return;
				}*/
				else if(wrongAttempts < 5){
					String[][] sample = dbOperation.getUsers("SELECT * FROM admintable");
					for(int i = 0; i<sample.length;i++){
						if(Username.getText().toString().equalsIgnoreCase(sample[i][1])) {
							if(Password.getText().toString().equalsIgnoreCase(sample[i][2])) {
								Intent intent = new Intent(getApplicationContext(), Monitoring_Page.class);
								startActivity(intent);
								finish();
							}
							else{
								Toast.makeText(getApplicationContext(), "Wrong Username/Password", Toast.LENGTH_SHORT).show();
							}
/*							else {
								wrongAttempts++;
								AlertDialog.Builder alert = new AlertDialog.Builder(WarActivity.this);
								alert.setTitle("Warning!!!");
								alert.setNeutralButton("Ok", null);
								alert.setMessage("You have "+(5-wrongAttempts)+" attempts remaining!!!");
								alert.show();
							}*/
						}
					}
				}
			}
		});
		
		Button cButton = (Button) findViewById(R.id.Cbutton);
		cButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Username.setText("");
				Password.setText("");
			}
		});
	}
}