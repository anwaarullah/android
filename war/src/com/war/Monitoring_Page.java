package com.war;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Monitoring_Page extends Activity implements SensorEventListener{

	private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	private PowerManager.WakeLock wl;
	private static final int REQUEST_DISCOVERY = 0x1;;
	private Handler handler = new Handler();
	private BluetoothSocket socket = null;
	private String str;
	private OutputStream outputStream;
	private InputStream inputStream;
	private StringBuffer sbu;
	private SensorManager sensorManager;
	TextView bluetoothStatusTV;
	TextView temperature;
	TextView human;
	TextView mine;
	ToggleButton automatic;
	ToggleButton forward;
	ToggleButton backward;
	boolean humanPresent = false;
	boolean minePresent = false;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.monitoring_page);
		bluetoothStatusTV = (TextView) findViewById(R.id.bluetoothStatus);
		Button enableBluetooth = (Button) findViewById(R.id.enableBluetooth);
		Button establishConnection = (Button) findViewById(R.id.establishConnection);
		Button refresh = (Button) findViewById(R.id.refresh);
		temperature = (TextView) findViewById(R.id.temperature);
		mine = (TextView) findViewById(R.id.mine);
		human = (TextView) findViewById(R.id.human);
		automatic = (ToggleButton)findViewById(R.id.automatic);
		forward = (ToggleButton) findViewById(R.id.forward);
		backward = (ToggleButton) findViewById(R.id.reverse);

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");

		sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);

		enableBluetooth.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(!bluetoothAdapter.isEnabled()) {
					Intent switchOnBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
					startActivity(switchOnBluetooth);
					bluetoothStatusTV.setText("Connection Status: Connected...");
				}
				else if(bluetoothAdapter.isEnabled()) {
					bluetoothStatusTV.setText("Connection Status: Connected...");					
				}
			}
		});

		establishConnection.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(), DiscoveryActivity.class);				
				Toast.makeText(getApplicationContext(), "Select BlueLink Device to Connect", Toast.LENGTH_LONG).show();
				startActivityForResult(intent, REQUEST_DISCOVERY);
			}
		});

		refresh.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String tmpStr = "A";
				byte bytes[] = tmpStr.getBytes();
				try {
					outputStream.write(bytes);
				} catch (IOException e) {	}

				if(str != null) {
					if(str.contains(".") && str.length() > 10) {
						String temp = str.substring(5, 10);
						temperature.setText("Temperature: " + temp);
//						Toast.makeText(getApplicationContext(), temp, Toast.LENGTH_LONG).show();
						str = "";
					}

					if(str.contains("Yes"))
						human.setText("Human Found: Yes!");
					else 
						human.setText("Human Found: No!");
					if(str.contains("True"))
						mine.setText("Mine Found: Yes!");
					else
						mine.setText("Mine Found: No!");
				}
				else {
					Toast.makeText(getApplicationContext(), "Please Refresh Again. Thanks!!!", Toast.LENGTH_LONG).show();
				}
				str = "";
			}
		});

		forward.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				if(isChecked) {
					writeCharToDevice("f");
					backward.setEnabled(false);
				}

				else if(!isChecked) {
					writeCharToDevice("F");
					backward.setEnabled(true);
				}
			}
		});
		backward.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				if(isChecked) {
					writeCharToDevice("b");
					forward.setEnabled(false);
				}
				else if(!isChecked) {
					writeCharToDevice("B");
					forward.setEnabled(true);
				}
			}
		});

	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode != REQUEST_DISCOVERY) {
			finish();
			return;
		}
		if (resultCode != RESULT_OK) {
			finish();
			return;
		}
		final BluetoothDevice device = data.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		new Thread() {
			public void run() {
				connect(device);
			};
		}.start();
	}

	protected void connect(BluetoothDevice device) {
		//BluetoothSocket socket = null;
		try {
			//Create a Socket connection: need the server's UUID number of registered
			socket = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));

			socket.connect();

			inputStream = socket.getInputStream();														
			outputStream = socket.getOutputStream();
			int read = -1;
			final byte[] bytes = new byte[20480];
			for (; (read = inputStream.read(bytes)) > -1;) {
				final int count = read;
				handler.post(new Runnable() {
					public void run() {

						StringBuilder b = new StringBuilder();
						for (int i = 0; i < count; ++i) {
							String s = Integer.toString(bytes[i]);
							b.append(s);
							b.append(",");
						}
						String s = b.toString();
						String[] chars = s.split(",");
						sbu = new StringBuffer();  
						for (int i = 0; i < chars.length; i++) {  
							sbu.append((char) Integer.parseInt(chars[i]));  
						}
						if(str != null)
						{	
							str += (sbu.toString());
						}
						else
						{
							str = sbu.toString();
						}
					}
				}); 
			}
		} catch (IOException e) {
			finish();
			return ;
		} finally {
			if (socket != null) {
				try {
					socket.close();	
					finish();
					return ;
				} catch (IOException e) {
				}
			}
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("EF-BTBee", ">>", e);
		}
	}
	public boolean isHumanPresent() {
		return humanPresent;
	}
	public void setHumanPresent(boolean humanPresent) {
		this.humanPresent = humanPresent;
	}
	public boolean isMinePresent() {
		return minePresent;
	}
	public void setMinePresent(boolean minePresent) {
		this.minePresent = minePresent;
	}
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
	}
	boolean right = true, left = true, straight = true, reverse = true;
	boolean rightSent = false, leftSent = false, straightSent = false, reverseSent = false;
	@Override
	public void onSensorChanged(SensorEvent event) {
		// check sensor type
		if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER && automatic.isChecked()){

			// assign directions
			float y=event.values[1];

			if ((y > 5) && right) {
				if(!leftSent) {
					//					Toast.makeText(getApplicationContext(), "Car Right Turn", Toast.LENGTH_SHORT).show();
					writeCharToDevice("r");
					leftSent = true;
				}
				right = false;
				left = true;
				rightSent = false;
			}
			else if((y < -5) && left) {
				if(!rightSent) {
					//					Toast.makeText(getApplicationContext(), "Car Left Turn", Toast.LENGTH_SHORT).show();
					writeCharToDevice("l");
					rightSent = true;
				}
				left = false;
				right = true;
				leftSent = false;
			}
			/*			if((z >= 5) && reverse) {
				if(!straightSent) {
					Toast.makeText(getApplicationContext(), "Car Forward", Toast.LENGTH_SHORT).show();		
					writeCharToDevice("f");
					straightSent= true;
				}
				straight = false;
				reverse = true;
				reverseSent = false;
			}
			else if((z < -5) && straight) {
				if(!reverseSent) {
					Toast.makeText(getApplicationContext(), "Car Backward", Toast.LENGTH_SHORT).show();
					writeCharToDevice("b");
					reverseSent= true;
				}
				reverse = false;
				straight = true;
				straightSent = false;
			}
			 */			if((y > 0) && (y < 5) && left && (!right)) {
				 if(leftSent) {
					 //					Toast.makeText(getApplicationContext(), "Car Right Turn Stop", Toast.LENGTH_SHORT).show();
					 writeCharToDevice("R");
				 }
				 right = true;
				 left = false;
				 leftSent = false;
			 }
			 else if((y < 0) && (y > -5) && right && (!left)) {
				 if(rightSent) {
					 //					Toast.makeText(getApplicationContext(), "Car Left Turn Stop", Toast.LENGTH_SHORT).show();
					 writeCharToDevice("L");
				 }
				 left = true;
				 right = false;
				 rightSent = false;
			 }
			 /*			if((z > -5) && reverse && (!straight)) {
				if(reverseSent) {
					Toast.makeText(getApplicationContext(), "Car Backward Stop", Toast.LENGTH_SHORT).show();
					writeCharToDevice("B");
					straightSent = false;
				}
				straight = true;
				reverse = false;
				reverseSent = false;
			}
			else if((z < 5) && straight && (!reverse)) {
				if(straightSent) {
					Toast.makeText(getApplicationContext(), "Car Forward Stop", Toast.LENGTH_SHORT).show();
					writeCharToDevice("F");
					reverseSent = false;
				}
				reverse = true;
				straight = false;
				straightSent = false;
			}
			  */		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		wl.release();
	};

	@Override
	protected void onResume() {
		super.onResume();
		wl.acquire();
	};
	protected void writeCharToDevice(String str) {
		String tmpStr = str;
		byte bytes[] = tmpStr.getBytes();
		try {
			outputStream.write(bytes);
		} catch (IOException e) {	}
	}
}
