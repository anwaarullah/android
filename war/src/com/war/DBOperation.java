package com.war;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class DBOperation {
	SampleDatabase database;

	public DBOperation() {
		database = WarActivity.database;
	}

	public void addUser (String userName, String password) {
		SQLiteDatabase tempDB = database.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put("name", userName);
		cv.put("password", password);
		tempDB.insert("admintable", null, cv);
	}

	public String [][] getUsers(String query) {
		String [][] array = null;
		SQLiteDatabase db = null;
		Cursor cursor = null;

		try {
			db = database.getReadableDatabase();
			cursor = db.rawQuery(query, null);
			if(!cursor.isFirst())
				cursor.moveToFirst();
			int rowCount = cursor.getCount();
			int columnCount = cursor.getColumnCount();
			array = new String[rowCount][columnCount];

			int index = 0;

			if(cursor.isFirst()) {
				do {
					for(int j=0; j<columnCount; j++) {
						array[index][j] = cursor.getString(j);
					}
					index++;	
				}while (cursor.moveToNext());
			}
		}
		catch (SQLiteException e) {
			Log.v("SQL Data Retrieval Error", e.getMessage());
		}
		finally {
			if(cursor != null)
				cursor.close();
			if(db != null)
				db.close();
		}
		return array;
	}
}
