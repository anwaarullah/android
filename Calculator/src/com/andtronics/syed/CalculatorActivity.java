package com.andtronics.syed;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CalculatorActivity extends Activity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		final AlertDialog.Builder alert = new AlertDialog.Builder(CalculatorActivity.this);
		alert.setTitle("Result:");
		alert.setNeutralButton("Ok", null);
		
		final Button add = (Button) findViewById(R.id.add);
		final Button subtract = (Button) findViewById(R.id.subtract);
		final Button multiply = (Button) findViewById(R.id.multiply);
		final Button divide = (Button) findViewById(R.id.divide);

		final EditText num1 = (EditText) findViewById(R.id.num1);
		final EditText num2 = (EditText) findViewById(R.id.num2);
		add.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				int result = 0;
				try {
					int number1 = Integer.parseInt(num1.getText().toString());
					int number2 = Integer.parseInt(num2.getText().toString());
					result = number1 + number2;
				}
				catch (NumberFormatException e) {
					Toast.makeText(getApplicationContext(), "Try to enter a valid number", 3000).show();
				}
				alert.setMessage(num1.getText().toString() + " + " + num2.getText().toString() + " = " + String.valueOf(result));
				alert.show();
			}
		});

		subtract.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				int result = 0;
				try {
					int number1 = Integer.parseInt(num1.getText().toString());
					int number2 = Integer.parseInt(num2.getText().toString());
					result = number1 - number2;
				}
				catch (NumberFormatException e) {
					Toast.makeText(getApplicationContext(), "Try to enter a valid number", 3000).show();
				}
				alert.setMessage(num1.getText().toString() + " - " + num2.getText().toString() + " = " + String.valueOf(result));
				alert.show();
			}			
		});
		multiply.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				int result = 0;
				try {
					int number1 = Integer.parseInt(num1.getText().toString());
					int number2 = Integer.parseInt(num2.getText().toString());
					result = number1 * number2;
				}
				catch (NumberFormatException e) {
					Toast.makeText(getApplicationContext(), "Try to enter a valid number", 3000).show();
				}
				alert.setMessage(num1.getText().toString() + " * " + num2.getText().toString() + " = " + String.valueOf(result));
				alert.show();
			}
		});

		divide.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				int result = 0;
				try {
					int number1 = Integer.parseInt(num1.getText().toString());
					int number2 = Integer.parseInt(num2.getText().toString());
					result = number1 / number2;
				}
				catch (NumberFormatException e) {
					Toast.makeText(getApplicationContext(), "Try to enter a valid number", 3000).show();
				}
				alert.setMessage(num1.getText().toString() + " / " + num2.getText().toString() + " = " + String.valueOf(result));
				alert.show();
			}	
		});
	}
}