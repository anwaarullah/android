package com.plot.demo;

import java.util.Date;
import java.util.Random;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;

public class DemoPlotActivity extends Activity {

	private static Random random = new Random();

	private static TimeSeries timeSeries;
	private static XYMultipleSeriesDataset dataset;
	private static XYMultipleSeriesRenderer renderer;
	private static XYSeriesRenderer rendererSeries;
	private static GraphicalView view;
	private static Thread mThread;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);     

		dataset = new XYMultipleSeriesDataset();

		renderer = new XYMultipleSeriesRenderer();
		renderer.setAxesColor(Color.BLUE);
		renderer.setAxisTitleTextSize(16);
		renderer.setChartTitle("Time");
		renderer.setChartTitleTextSize(15);
		renderer.setFitLegend(true);
		renderer.setGridColor(Color.LTGRAY);
		renderer.setPanEnabled(true, true);
		renderer.setPointSize(10);
		renderer.setXTitle("Time");
		renderer.setYTitle("Number");
		renderer.setMargins( new int []{20, 30, 15, 0});
		renderer.setZoomButtonsVisible(true);
		renderer.setBarSpacing(10);
		renderer.setShowGrid(true);


		rendererSeries = new XYSeriesRenderer();
		rendererSeries.setColor(Color.RED);
		renderer.addSeriesRenderer(rendererSeries);
		rendererSeries.setFillPoints(true);
		rendererSeries.setPointStyle(PointStyle.CIRCLE);

		timeSeries = new TimeSeries("Random");
		mThread = new Thread(){
			public void run(){
				while(true){
					try {
						Thread.sleep(2000L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					timeSeries.add(new Date(), random.nextInt(10));
					view.repaint();
				}
			}
		};
		mThread.start();
	}

	@Override
	protected void onStart() {
		super.onStart();
		dataset.addSeries(timeSeries);
		view = ChartFactory.getTimeChartView(this, dataset, renderer, "Test");
		view.refreshDrawableState();
		view.repaint();
		setContentView(view);    
	}
}