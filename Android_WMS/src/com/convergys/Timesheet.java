/**
 * 
 */
package com.convergys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * @author sanw9912
 *
 */
public class Timesheet extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timesheet);
		
		Button but1 = (Button) findViewById(R.id.button1);
		Button but3 = (Button) findViewById(R.id.button3);
		
		but1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(), Home_Page.class);
				startActivityForResult(myIntent, 0);
			}
		});
		
		but3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(), ItemPage.class);
				startActivityForResult(myIntent,0);
			}
		});
	}
	
}
