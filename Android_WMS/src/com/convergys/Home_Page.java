package com.convergys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Home_Page extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button goToActivity2 = (Button) findViewById(R.id.button2);
        Button goToActivity3 = (Button) findViewById(R.id.button3);
        Button calBut = (Button) findViewById(R.id.calendarbutton);
        
        goToActivity2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(), Timesheet.class);
				startActivityForResult(myIntent, 0);
			}
		});
        
        goToActivity3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				Intent myIntent = new Intent(view.getContext(), ItemPage.class);
				startActivityForResult(myIntent, 0);
			}
		});
        
        calBut.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent calendarIntent = new Intent();
				calendarIntent.setClassName("com.android.calendar","com.android.calendar.AgendaActivity");
				startActivityForResult(calendarIntent, 0);
			}
		});
    }
}