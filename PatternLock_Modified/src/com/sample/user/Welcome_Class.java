package com.sample.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Welcome_Class extends Activity {
	ImageButton continueButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		continueButton = (ImageButton) findViewById(R.id.continueButton);
		
		continueButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), SpeechToTextActivity.class);
				startActivity(intent);
			}
		});
	}
}
