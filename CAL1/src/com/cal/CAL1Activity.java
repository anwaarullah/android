package com.cal;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CAL1Activity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Button addbut=(Button) findViewById(R.id.addbutton);
		Button subbut=(Button) findViewById(R.id.subbutton);
		Button mulbut=(Button) findViewById(R.id.mulbutton);
		Button divbut=(Button) findViewById(R.id.divbutton);
		final EditText first=(EditText) findViewById(R.id.firstinput);
		final EditText second=(EditText) findViewById(R.id.secondinput);
		addbut.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				int result = 0;
				try {
					int number1 = Integer.parseInt(first.getText().toString());
					int number2 = Integer.parseInt(second.getText().toString());
					result = number1 + number2;
				}
				catch (NumberFormatException e) {
				}
				Toast.makeText(getApplicationContext(), "Result:" +result , Toast.LENGTH_LONG).show();
			}
		});
		subbut.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub


			}
		});
		mulbut.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub


			}
		});
		divbut.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub


			}
		});
	}
}