package com.sample.bling;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;

public class MainClass extends JFrame{

	private static final long serialVersionUID = 1L;
	private static JButton button = new JButton("Send Text");
	private static JTextField textField = new JTextField("Enter Data");
	
	/**
	 * @param args
	 */
		public static void main(String[] args) {
			MainClass mainClass = new MainClass();
			System.out.println("Welcome to Java");
			button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					sendGCMMessage(textField.getText());
				}
			});
			textField.setSize(300, 50);
			button.setSize(20, 20);
			mainClass.setTitle("GCM Client for pushing TidBits");
			mainClass.add(BorderLayout.CENTER, textField);
			mainClass.add(BorderLayout.EAST, button);
			mainClass.setSize(500, 100);
			mainClass.setVisible(true);

	}
	
	public static void sendGCMMessage(String text) {
		try {
			
			Sender sender = new Sender("AIzaSyDDGdc0xsoYSVCwbyDC7XEk5bNDsgYnW9g");
			
			ArrayList<String> devicesList = new ArrayList<String>();
			
			devicesList.add("APA91bEX5NvXkKPczLTdD4QGLhXSxwEiHv13FsiZyV4MyDY4tVl36pLR-UX-aN222gx44MLDQiVSeWpN_Cm2ifpPE4UAEMmVmHTjXhQ13pC9x7q6MOQMYiUdUcpTR8XP_SxZrsC9EUb9RSxbCrHHojGLGNcBcl7vtw");

			Message message = new Message.Builder().collapseKey("1").timeToLive(3).delayWhileIdle(true).addData("message", text).build();
			
			MulticastResult result = sender.send(message, devicesList, 1);
			sender.send(message, devicesList, 1);
			
			System.out.println(result.toString());
			
			if(result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if(canonicalRegId != 0) {
					
				}
				else {
					int error = result.getFailure();
					System.out.println(error);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
