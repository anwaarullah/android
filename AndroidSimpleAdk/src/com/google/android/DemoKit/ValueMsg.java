package com.google.android.DemoKit;

public class ValueMsg {

	private String message;
	
	public ValueMsg(String msg) {
		this.message = msg;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
