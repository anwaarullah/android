package com.google.android.DemoKit;

import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;
import android.widget.ToggleButton;

public class DemoKitPhone extends BaseActivity {
	static final String TAG = "DemoKitPhone";
	private final byte digitalPort = 0x1;
	private final byte outputHighLow = 0x0;
	private final byte low = 0x0;

	private ToggleButton dHighLow7;
	private TextView input;

	private OnCheckedChangeListener highLowChangeListener = new HighLowChangeListener();

	@Override
	protected void hideControls() {
		super.hideControls();
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		input = (TextView) findViewById(R.id.input);
	}

	protected void showControls() {
		super.showControls();

		dHighLow7.setTag((byte) 0x7);
		dHighLow7.setOnCheckedChangeListener(highLowChangeListener);
	}

	private class HighLowChangeListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			byte portByte = (Byte) buttonView.getTag();
			if (isChecked) {
				input.setText(Integer.toString(readCommand()));
				Log.i(TAG, "message send: digital pin " + portByte + " HIGH");
			} else {
				Toast.makeText(getApplicationContext(), "Switch on the Toggle Button to get the val", Toast.LENGTH_SHORT).show();
			}
		}
	}
}