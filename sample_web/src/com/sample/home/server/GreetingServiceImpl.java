package com.sample.home.server;

import java.io.IOException;
import java.util.ArrayList;

import com.sample.home.client.GreetingService;
import com.sample.home.shared.FieldVerifier;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
		GreetingService {

	public String greetServer(String input) throws IllegalArgumentException {
		// Verify that the input is valid. 
		if (!FieldVerifier.isValidName(input)) {
			// If the input is not valid, throw an IllegalArgumentException back to
			// the client.
			throw new IllegalArgumentException(
					"Name must be at least 4 characters long");
		}

		Sender sender = new Sender("AIzaSyByhn8tW-b96E762N-jFqqXZrTNJQvzTLA");
		ArrayList<String> devicesList = new ArrayList<String>();
		devicesList.add("APA91bGQVfqcBtl7T2gTl4_MEOqSIfPstmeNPd2zCTqufa6d51zRwlYf6VIXUMdlO6D2Zhk8kPkWcuiwyrsqyN8vbsAHmF3SJmU6nixfBH2zSC4uwO5SXB8u8WkbA-bYHA1AwZats1DlNzwkrgp499_SGdUWpkqsvA");
		Message message = new Message.Builder().collapseKey("1").timeToLive(3).delayWhileIdle(true).addData("message", "Assalamualaikum, Alhamdulillah").build();
		try {
		MulticastResult result = sender.send(message, devicesList, 1);
			sender.send(message, devicesList, 1);
			
			if (result.getResults() != null) {
				int canonicalRegId = result.getCanonicalIds();
				if (canonicalRegId != 0) {
				}
			} else {
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String serverInfo = getServletContext().getServerInfo();
		String userAgent = getThreadLocalRequest().getHeader("User-Agent");

		// Escape data from the client to avoid cross-site script vulnerabilities.
		input = escapeHtml(input);
		userAgent = escapeHtml(userAgent);

		return "Hello, " + input + "!<br><br>I am running " + serverInfo
				+ ".<br><br>It looks like you are using:<br>" + userAgent;
	}

	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;");
	}
}
