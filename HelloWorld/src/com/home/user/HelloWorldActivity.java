package com.home.user;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HelloWorldActivity extends Activity {
	EditText inputText;
	EditText inputNumber;
    int square = 0;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        inputText = (EditText) findViewById(R.id.inputText);
        inputNumber = (EditText) findViewById(R.id.inputNumber);
        Button inputButton  = (Button) findViewById(R.id.inputButton);
        
        inputButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				square = Integer.parseInt(inputNumber.getText().toString());
				square = square*square;
				Toast.makeText(getApplicationContext(), inputText.getText().toString() +"\n"+ Integer.toString(square), Toast.LENGTH_LONG).show();
			}
		});
    }
}