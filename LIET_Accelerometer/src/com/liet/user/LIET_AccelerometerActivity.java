package com.liet.user;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

public class LIET_AccelerometerActivity extends Activity implements SensorEventListener {

	TextView xCordinate;
	TextView yCordinate;
	TextView zCordinate;

	private SensorManager sensorManager;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		xCordinate = (TextView) findViewById(R.id.x);
		yCordinate = (TextView) findViewById(R.id.y);
		zCordinate = (TextView) findViewById(R.id.z);
		
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), 
				SensorManager.SENSOR_DELAY_NORMAL);
	}
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
	}
	@Override
	public void onSensorChanged(SensorEvent event) {
		
		if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
			
			float x=event.values[0];
			float y=event.values[1];
			float z=event.values[2];
			
			xCordinate.setText("X: " + x);
			yCordinate.setText("Y: " + y);
			zCordinate.setText("Z: " + z);
		}
	}
}
