package com.sample.test;

import android.app.Activity;
import android.app.Notification;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Testing_FeaturesActivity extends Activity {
	TextToSpeech talker;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final Toast toast = Toast.makeText(getApplicationContext(), "This is a sample Toast message", Toast.LENGTH_SHORT);
        final Notification sampleNot = new Notification(R.drawable.ic_launcher, "This is a sample Application", System.currentTimeMillis());
        sampleNot.number++;
        
        Button button = (Button) findViewById(R.id.button1);
 
        button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				toast.show();
				sampleNot.notify();
			}
		});
    }
}