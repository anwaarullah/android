package com.sample.croma;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Croma_ExampleActivity extends Activity {
	Button touchMe;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        touchMe = (Button) findViewById(R.id.button1);
        touchMe.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "Thanks ALLAH for everything!!!", Toast.LENGTH_LONG).show();
			}
		});
    }
}