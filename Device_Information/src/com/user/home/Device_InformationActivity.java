package com.user.home;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Device_InformationActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Button deviceModelButton = (Button) findViewById(R.id.deviceModel);
		deviceModelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Toast.makeText(getApplicationContext(), android.os.Build.MODEL.toString() + "--" + 
						android.os.Build.VERSION.RELEASE + android.os.Build.BOOTLOADER + "--" + 
						android.os.Build.MANUFACTURER, Toast.LENGTH_LONG).show();			
			}
		});
	}
}