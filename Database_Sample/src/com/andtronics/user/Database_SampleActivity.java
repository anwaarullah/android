package com.andtronics.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Database_SampleActivity extends Activity {
	public static SampleDatabase database;
	DBOperation dbOperation;
	private EditText userName, userPassword;
	Button createContact, showRecord, login;
	/** Called when the activity is first created. */
	@Override

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);        

		database = new SampleDatabase(getApplicationContext(), "admin.db", null, 1);
		database.checkDatabase();

		dbOperation = new DBOperation();

		userName = (EditText) findViewById(R.id.nameTextField);
		userPassword = (EditText) findViewById(R.id.passwordTextField);

		createContact = (Button) findViewById(R.id.save);
		createContact.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dbOperation.addUser(userName.getText().toString(), userPassword.getText().toString());
				Toast.makeText(getApplicationContext(), "Record Saved", Toast.LENGTH_LONG).show();
			}
		});

		showRecord = (Button) findViewById(R.id.retrieve);

		showRecord.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String[][] sample = dbOperation.getUsers("SELECT * FROM admintable");
				for(int i = 0; i<sample.length;i++){
					Toast.makeText(getApplicationContext(), "UserName: " + sample[i][1] + "\nPassword: " + sample[i][2], Toast.LENGTH_LONG).show();	
				}
			}
		});

		login = (Button) findViewById(R.id.login);
		login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String[][] sample = dbOperation.getUsers("SELECT * FROM admintable");
				for(int i = 0; i<sample.length;i++){
					if(userName.getText().toString().equalsIgnoreCase(sample[i][1]))
						if(userPassword.getText().toString().equalsIgnoreCase(sample[i][2])) {
							Intent intent = new Intent(getApplicationContext(), Next_Page.class);
							startActivity(intent);
						}
				}	
			}
		});
	}
}