package com.andtronics.user;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SampleDatabase extends SQLiteOpenHelper{

	private static final String DB_PATH = "/data/data/com.andtronics.user/databases/";
	private static final String DB_NAME = "admin.db";

	public SampleDatabase(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.v("onCreate DB", "Inside the onCreate Method of DB");
		try {
			db.execSQL("CREATE TABLE admintable " + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, password TEXT)");
		}
		catch (SQLiteException e) {
			Log.v("Create Table Exception: ",e.getMessage());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { }

	public boolean checkDatabase() {
		SQLiteDatabase checkDB = null;

		try {
			String myPath = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
		}
		catch (SQLiteException e) {
			Log.v("Database Doesn't exist", e.getMessage());
			// TODO: handle exception
		}
		if(checkDB != null)
			return true;
		else
			return false;
	}
}
