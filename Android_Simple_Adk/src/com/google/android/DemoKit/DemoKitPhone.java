package com.google.android.DemoKit;

import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

public class DemoKitPhone extends BaseActivity {
	static final String TAG = "DemoKitPhone";
	private final byte digitalPort = 0x1;
	private final byte outputHighLow = 0x0;
	private final byte low = 0x0;
	private final byte high = 0x1;

	private ToggleButton dHighLow7;
	private TextView sensorValue;
	private ToggleButton button;
	private TextView input;
	

	private OnCheckedChangeListener highLowChangeListener = new HighLowChangeListener();
	private OnCheckedChangeListener buttonHighLowChangeListener = new ButtonHighLowChangeListener();

	@Override
	protected void hideControls() {
		super.hideControls();
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	protected void showControls() {
		super.showControls();

		dHighLow7 = (ToggleButton) findViewById(R.id.dPinHighLow7);
		dHighLow7.setTag((byte) 0x7);
		dHighLow7.setOnCheckedChangeListener(highLowChangeListener);
		sensorValue = (TextView) findViewById(R.id.textView1);
		button = (ToggleButton) findViewById(R.id.toggleButton1);
		button.setTag((byte) 0x8);
		button.setOnCheckedChangeListener(buttonHighLowChangeListener);
		input = (TextView) findViewById(R.id.input);
	}

	private class HighLowChangeListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			byte portByte = (Byte) buttonView.getTag();
			if (isChecked) {
				sendCommand(digitalPort, portByte, outputHighLow, high);
				Log.i(TAG, "message send: digital pin " + portByte + " HIGH");
			} else {
				sendCommand(digitalPort, portByte, outputHighLow, low);
				Log.i(TAG, "message send: digital pin " + portByte + " LOW");
			}
		}
	}
	
	private class ButtonHighLowChangeListener implements OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			byte portByte = (Byte) buttonView.getTag();
			if (isChecked) {
				input.setText(readCommand(digitalPort, portByte));
			} else {
			}
		}
	}
}