package com.sample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SampleActivity extends Activity {
	TextView NAME;
	TextView PASSWORD;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		NAME=(TextView)findViewById(R.id.textView1);
		PASSWORD=(TextView)findViewById(R.id.textView2);

		Button b1=(Button) findViewById(R.id.button1);
		b1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(NAME.getText().length()==0)
				{
					Toast.makeText(getApplicationContext(), "PLEASE ENTER THE NAME", Toast.LENGTH_LONG).show();
					return;
				}
				else if (PASSWORD.getText().length()==0) {
					Toast.makeText(getApplicationContext(), "PLEASE ENTER THE PASSWORD", Toast.LENGTH_LONG).show();
					return;
				}
/*				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
				startActivity(intent);
*/				
				Intent intent = new Intent(v.getContext(), Sample1.class);
				startActivity(intent);

				Toast.makeText(getApplicationContext(), "Button Clicked", Toast.LENGTH_LONG).show();
			}
		});
	}
}