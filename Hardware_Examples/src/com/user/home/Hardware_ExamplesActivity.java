package com.user.home;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Hardware_ExamplesActivity extends Activity {
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    	Button openCamera = (Button) findViewById(R.id.startCamera);
        openCamera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
//				Intent intent = new Intent(Intent.ACTION_CAMERA_BUTTON);
				Camera cam = Camera.open();
				cam.startPreview();
				
//				startActivity(intent);
//				startActivityForResult(intent, 0);
			}
		});
    }
}