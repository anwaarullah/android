package com.home.accelerometer;

import android.app.Activity;
import android.os.Bundle;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Accelerometer_TestActivity extends Activity implements SensorEventListener {
	private SensorManager sensorManager;

	TextView xCoor; // declare X axis object
	TextView yCoor; // declare Y axis object
	TextView zCoor; // declare Z axis object
	ToggleButton automatic;

	/** Called when the activity is first created. */
	@Override

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		xCoor=(TextView)findViewById(R.id.xcoor); // create X axis object
		yCoor=(TextView)findViewById(R.id.ycoor); // create Y axis object
		zCoor=(TextView)findViewById(R.id.zcoor); // create Z axis object
		automatic = (ToggleButton)findViewById(R.id.automatic);
		sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
		// add listener. The listener will be HelloAndroid (this) class
		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);
		
	}
	public void onAccuracyChanged(Sensor sensor,int accuracy){

	}

	boolean right = true, left = true, straight = true, reverse = true;
	boolean rightSent = false, leftSent = false, straightSent = false, reverseSent = false;
	public void onSensorChanged(SensorEvent event){
		// check sensor type
		if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER && automatic.isChecked()){

			// assign directions
			float x=event.values[0];
			float y=event.values[1];
			float z=event.values[2];

			xCoor.setText("X: "+x);
			yCoor.setText("Y: "+y);
			zCoor.setText("Z: "+z);
 			if ((y > 5) && right) {
				if(!leftSent) {
					Toast.makeText(getApplicationContext(), "Car Right Turn", Toast.LENGTH_SHORT).show();
					leftSent = true;
				}
				right = false;
				left = true;
				rightSent = false;
			}
			else if((y < -5) && left) {
				if(!rightSent) {
					Toast.makeText(getApplicationContext(), "Car Left Turn", Toast.LENGTH_SHORT).show();
					rightSent = true;
				}
				left = false;
				right = true;
				leftSent = false;
			}
			if((z >= 5) && reverse) {
				if(!straightSent) {
					Toast.makeText(getApplicationContext(), "Car Forward", Toast.LENGTH_SHORT).show();					
					straightSent= true;
				}
				straight = false;
				reverse = true;
				reverseSent = false;
			}
			else if((z < -5) && straight) {
				if(!reverseSent) {
					Toast.makeText(getApplicationContext(), "Car Backward", Toast.LENGTH_SHORT).show();
					reverseSent= true;
				}
				reverse = false;
				straight = true;
				straightSent = false;
			}
			if((y > 0) && (y < 5) && left && (!right)) {
				if(leftSent) {
					Toast.makeText(getApplicationContext(), "Car Right Turn Stop", Toast.LENGTH_SHORT).show();
				}
				right = true;
				left = false;
				leftSent = false;
			}
			else if((y < 0) && (y > -5) && right && (!left)) {
				if(rightSent) {
					Toast.makeText(getApplicationContext(), "Car Left Turn Stop", Toast.LENGTH_SHORT).show();
				}
				left = true;
				right = false;
				rightSent = false;
			}
			if((z > -5) && reverse && (!straight)) {
				if(reverseSent) {
					Toast.makeText(getApplicationContext(), "Car Backward Stop", Toast.LENGTH_SHORT).show();
					straightSent = false;
				}
				straight = true;
				reverse = false;
				reverseSent = false;
			}
			else if((z < 5) && straight && (!reverse)) {
				if(straightSent) {
					Toast.makeText(getApplicationContext(), "Car Forward Stop", Toast.LENGTH_SHORT).show();
					reverseSent = false;
				}
				reverse = true;
				straight = false;
				straightSent = false;
			}
		}
	}
}