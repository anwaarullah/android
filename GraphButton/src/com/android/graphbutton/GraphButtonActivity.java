package com.android.graphbutton;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

public class GraphButtonActivity extends Activity {
	plotDynamic dynamic;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout graphLayout = new LinearLayout(this);
		graphLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT); // Verbose!

		float[] xvalues = new float[] { -1.0f, 1.0f, 2.0f, 3.0f , 4.0f, 5.0f, 6.0f };
		float[] yvalues = new float[] { 15.0f, 2.0f, 0.0f, 2.0f, -2.5f, -1.0f , -28.0f };
		plot2d graph = new plot2d(this, xvalues, yvalues, 1);
		plotDynamic dynaPlot = new plotDynamic(getApplicationContext());
		graphLayout.addView(dynaPlot, lp);
		for(int i = 0; i < 100; i++) {
			dynaPlot.addData((float)Math.random()*10.0f);		
			Log.d("Plot", Float.toString((float)Math.random()*10.0f));
		}

		//graphLayout.addView(graph, lp);
		setContentView(graphLayout);
	}
}