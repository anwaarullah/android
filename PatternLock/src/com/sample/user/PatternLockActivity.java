package com.sample.user;

import group.pals.android.lib.ui.lockpattern.LockPatternActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.widget.Toast;

public class PatternLockActivity extends Activity implements OnInitListener{
	private static final int _ReqSignIn = 1;
	String pattern = "3211650032ae0bd91b9de4ab1fa27401ab03e67e";

	TextToSpeech talker; // Object for TextToSpeech

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		talker = new TextToSpeech(this, this);
		Intent intent = new Intent(getApplicationContext(), LockPatternActivity.class);
		intent.putExtra(LockPatternActivity._Mode, LockPatternActivity.LPMode.ComparePattern);
		intent.putExtra(LockPatternActivity._Pattern, pattern);
		startActivityForResult(intent, _ReqSignIn);
	}

	public void onInit(int status) {
		say("This is a very boring workshop. I'm not understandig anything. Thanks!!!");
	}
	@Override
	public void onDestroy() {
		if (talker != null) {
			talker.stop();
			talker.shutdown();
		}
		super.onDestroy();
	}
	public void say(String text2say){
		talker.speak(text2say, TextToSpeech.QUEUE_FLUSH, null);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case _ReqSignIn:
			if (resultCode == RESULT_OK) {
				Intent intent = new Intent(getApplicationContext(), SpeechToTextActivity.class);
				startActivityForResult(intent, 0);
				finish();
			} else {
				// signing in failed
				Toast.makeText(getApplicationContext(), "False", Toast.LENGTH_LONG).show();
			}
			break;
		}
	}
}