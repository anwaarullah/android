package com.sample.user;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class SpeechToTextActivity extends Activity {
	ImageButton speakNowButton;
	private boolean deviceConnected = false;
	private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	private PowerManager.WakeLock wl;
	private static final int REQUEST_DISCOVERY = 0x1;;
	private Handler handler = new Handler();
	private BluetoothSocket socket = null;
	private String str;
	private OutputStream outputStream;
	private InputStream inputStream;
	private StringBuffer sbu;
	Button establishConnection;
	protected ArrayList<String> matches;
	private static final int REQUEST_CODE = 1234;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.speech);
		speakNowButton = (ImageButton) findViewById(R.id.speakNowButton);
		establishConnection = (Button) findViewById(R.id.connectToBluetooth);

		if(bluetoothAdapter.isEnabled())
			establishConnection.setText("Connect to Device");

		PackageManager pm = getPackageManager();
		List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH),0);
		if(activities.size() == 0) {
			speakNowButton.setEnabled(false);
		}
		speakNowButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				startVoiceRecognitionActivity();
			}
		});

		Button fanOn = (Button) findViewById(R.id.fanOn);
		fanOn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				writeCharToDevice("2");
			}
		});
		Button fanOff = (Button) findViewById(R.id.fanOff);
		fanOff.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				writeCharToDevice("5");
			}
		});

		Button lightOn = (Button) findViewById(R.id.lampOn);
		lightOn.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				writeCharToDevice("1");
			}
		});

		Button lightOff = (Button) findViewById(R.id.lampOff);
		lightOff.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				writeCharToDevice("4");
			}
		});

		Button loadOn = (Button) findViewById(R.id.loadOn);
		loadOn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				writeCharToDevice("3");
			}
		});

		Button loadOff = (Button) findViewById(R.id.loadOff);
		loadOff.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				writeCharToDevice("6");
			}
		});

		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");

		establishConnection.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				if(!bluetoothAdapter.isEnabled()) {
					Intent switchOnBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
					startActivity(switchOnBluetooth);
					establishConnection.setText("Connect to Device");
				}
				else if(bluetoothAdapter.isEnabled()) {
					Intent intent = new Intent(getApplicationContext(), DiscoveryActivity.class);				
					startActivityForResult(intent, REQUEST_DISCOVERY);
				}
				else if(isDeviceConnected())
					try {
						socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		});
	}

	protected void connect(BluetoothDevice device) {
		//BluetoothSocket socket = null;
		try {
			//Create a Socket connection: need the server's UUID number of registered
			socket = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));

			socket.connect();

			inputStream = socket.getInputStream();														
			outputStream = socket.getOutputStream();
			int read = -1;
			final byte[] bytes = new byte[20480];
			for (; (read = inputStream.read(bytes)) > -1;) {
				final int count = read;
				handler.post(new Runnable() {
					public void run() {

						StringBuilder b = new StringBuilder();
						for (int i = 0; i < count; ++i) {
							String s = Integer.toString(bytes[i]);
							b.append(s);
							b.append(",");
						}
						String s = b.toString();
						String[] chars = s.split(",");
						sbu = new StringBuffer();  
						for (int i = 0; i < chars.length; i++) {  
							sbu.append((char) Integer.parseInt(chars[i]));  
						}
						if(str != null)
						{	
							str += (sbu.toString());
						}
						else
						{
							str = sbu.toString();
						}
					}
				}); 
			}
		} catch (IOException e) {
			finish();
			return ;
		} finally {
			if (socket != null) {
				try {
					socket.close();	
					finish();
					return ;
				} catch (IOException e) {
				}
			}
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		try {
			if(isDeviceConnected())
				socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}
	@Override
	protected void onPause() {
		super.onPause();
		wl.release();
	};

	@Override
	protected void onResume() {
		super.onResume();
		wl.acquire();
	};
	protected void writeCharToDevice(String str) {
		if(isDeviceConnected()) {
			String tmpStr = str;
			byte bytes[] = tmpStr.getBytes();
			try {
				outputStream.write(bytes);
			} catch (IOException e) {	}
		}
	}

	private void startVoiceRecognitionActivity() {
		Intent intent  = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Voice Recognition Demo...");
		startActivityForResult(intent, REQUEST_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == REQUEST_DISCOVERY && resultCode == RESULT_OK) {
			establishConnection.setText("Connected to Device");
			final BluetoothDevice device = data.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			new Thread() {
				public void run() {
					connect(device);
				};
			}.start();
			setDeviceConnected(true);
			establishConnection.setText("Disconnect!");
		}

		else if(requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null) {
			matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
			for(int i = 0; i < matches.size(); i++) {
				if(matches.get(i).contains("yellow lamp on")) {
					writeCharToDevice("1");
				}
				else if(matches.get(i).contains("yellow lamp off")) {
					writeCharToDevice("4");
				}
				else if(matches.get(i).contains("blue lamp on")) {
					writeCharToDevice("2");
				}
				else if(matches.get(i).contains("blue lamp off")) {
					writeCharToDevice("5");
				}
				else if(matches.get(i).contains("red lamp on")) {
					writeCharToDevice("3");
				}
				else if(matches.get(i).contains("red lamp off")) {
					writeCharToDevice("6");
				}
			}
		}

		else if (requestCode != REQUEST_DISCOVERY || resultCode != RESULT_OK || requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null) {
			//			finish();
			Toast.makeText(getApplicationContext(), "Error processing request. Please try again!!!", Toast.LENGTH_LONG).show();
			return;
		}
	}
	public boolean isDeviceConnected() {
		return deviceConnected;
	}

	public void setDeviceConnected(boolean deviceConnected) {
		this.deviceConnected = deviceConnected;
	}
}