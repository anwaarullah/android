package com.robot.user;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import android.widget.ToggleButton;

import com.android.future.usb.UsbAccessory;
import com.android.future.usb.UsbManager;

public class OSI_DemoActivity extends Activity {

	FileInputStream mInputStream;
	FileOutputStream mOutputStream;

	private ToggleButton lampToggleButton;

	private static final String ACTION_USB_PERMISSION = "com.robot.user.action.USB_PERMISSION";

	private UsbManager mUsbManager;
	private UsbAccessory mUsbAccessory;
	private ParcelFileDescriptor mFileDescriptor;
	private PendingIntent mPermissionIntent;

	private boolean mPersmissionRequestPending;

	private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(ACTION_USB_PERMISSION.equals(action)) {
				synchronized (this) {
					UsbAccessory accessory = UsbManager.getAccessory(intent);
					if(intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
						openAccessory(accessory);
					}
					else {
						Log.d("Arduino ADK Example", "Permission Denied for Accessory" + accessory);
					}
					mPersmissionRequestPending = false;
				}
			} else if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) {
				UsbAccessory accessory = UsbManager.getAccessory(intent);
				if(accessory != null && accessory.equals(mUsbAccessory)) {
					closeAccessory();
				}
			}
		}
	};

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mUsbManager = UsbManager.getInstance(this);
		mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
		IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
		filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
		registerReceiver(mUsbReceiver, filter);

		if(getLastNonConfigurationInstance() != null) {
			mUsbAccessory = (UsbAccessory) getLastNonConfigurationInstance();
			openAccessory(mUsbAccessory);
		}

		setContentView(R.layout.main);
		lampToggleButton = (ToggleButton) findViewById(R.id.lampToggleButton);
		lampToggleButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				lampOnOffFunction();
			}
		});
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		if(mUsbAccessory != null) {
			return mUsbAccessory;
		}
		else {
			return super.onRetainNonConfigurationInstance();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if(mInputStream != null && mOutputStream != null) {
			return;
		}
		
		UsbAccessory[] accessories = mUsbManager.getAccessoryList();
		UsbAccessory accessory = (accessories == null ? null : accessories[0]);
		if(accessory != null) {
			if(mUsbManager.hasPermission(accessory)) {
				openAccessory(accessory);
			}
			else {
				synchronized (mUsbReceiver) {
					if(!mPersmissionRequestPending) {
						mUsbManager.requestPermission(accessory, mPermissionIntent);
						mPersmissionRequestPending = true;
					}
				}
			}
		}
		else {
			Log.d("Arduino ADK", "mAccessory is Null");
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		closeAccessory();
	};
	
	@Override
	protected void onDestroy() {
		unregisterReceiver(mUsbReceiver);
		super.onDestroy();
	}
		
	protected void closeAccessory() {
		if(mFileDescriptor != null) {
			try {
				mFileDescriptor.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally{
				mFileDescriptor = null;
				mUsbAccessory = null;
			}
		}
	}

	protected void openAccessory(UsbAccessory accessory) {
		mFileDescriptor = mUsbManager.openAccessory(accessory);
		if(mFileDescriptor != null) {
			mUsbAccessory = accessory;
			FileDescriptor fd = mFileDescriptor.getFileDescriptor();
			mInputStream = new FileInputStream(fd);
			mOutputStream = new FileOutputStream(fd);
			Log.d("Arduino ADK", "Accessory Opened");
		}
		else {
			Log.d("Arduino ADK", "Accessory Open Failed");
		}
	}

	public void lampOnOffFunction() {
		byte[] buffer = new byte[1];

		if(lampToggleButton.isChecked()) {
			buffer[0] = (byte)1;
		}
		else {
			buffer[0] = (byte)0;
		}

		if(mOutputStream != null) {
			try {
				mOutputStream.write(buffer);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e("Arduino ADK Example" , "Write Failed", e);
			}
		}
	}

}