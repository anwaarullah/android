package com.general.com;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Main extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static int i = 0;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Welcome to Java Programming");
		
		JFrame frame = new JFrame("My Application");
		JButton button = new JButton("Click Me");
		button.setSize(20, 60);
		final JLabel label = new JLabel();
		label.setSize(60, 20);
		label.setText("Count = " + i);
		
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				label.setText("Count = " + ++i);
			}
		});
		
		frame.add(label);
		frame.add(button);
		frame.pack();
		frame.setVisible(true);
		
	}

}
