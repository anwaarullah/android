package com.android.test;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class Source_AndroidActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        LockPatternUtils util = new LockPatternUtils(getApplicationContext());
        Toast.makeText(getApplicationContext(), "Test Toast", Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(), "Permanently Locked: " + util.isPermanentlyLocked(), Toast.LENGTH_LONG).show();
    }
}