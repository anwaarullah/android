package com.iteadstudio;

import android.app.Application;
import android.bluetooth.BluetoothDevice;

public class SocketApplication  {

	private BluetoothDevice device = null;

	public BluetoothDevice getDevice() {
		return device;
	}

	public void setDevice(BluetoothDevice device) {
		this.device = device;
	}
	
}
