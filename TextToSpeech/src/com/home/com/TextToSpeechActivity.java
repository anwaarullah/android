package com.home.com;

import android.app.Activity;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;

public class TextToSpeechActivity extends Activity implements OnInitListener {
    /** Called when the activity is first created. */
	TextToSpeech talker;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        talker = new TextToSpeech(this, this);
    }
    public void say(String text2say){
    	talker.speak(text2say, TextToSpeech.QUEUE_FLUSH, null);
    }

	@Override
	public void onInit(int status) {
		
		say("Good Morning.");
		
	}
	
	@Override
	public void onDestroy() {
		if (talker != null) {
			talker.stop();
			talker.shutdown();
		}

		super.onDestroy();
	}
}