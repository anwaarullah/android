package com.home.user;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class SpeechToTextActivity extends Activity {
	Button speakNowButton;
	protected ArrayList<String> matches;
	private static final int REQUEST_CODE = 1234;
	private ListView wordsList;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speech);
        speakNowButton = (Button) findViewById(R.id.speakNowButton);
        wordsList = (ListView) findViewById(R.id.list);
        
        PackageManager pm = getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH),0);
        if(activities.size() == 0) {
        	speakNowButton.setEnabled(false);
        	speakNowButton.setText("Recognizer not present...");
        }
        speakNowButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				startVoiceRecognitionActivity();
			}
		});
    }

    private void startVoiceRecognitionActivity() {
		Intent intent  = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Voice Recognition Demo...");
		startActivityForResult(intent, REQUEST_CODE);
	}
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if(requestCode == REQUEST_CODE && resultCode == RESULT_OK && data != null) {
    		matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
    		wordsList.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, matches));
    	}
    	if(matches.get(0).contains("example")) {
    		Toast.makeText(getApplicationContext(), matches.get(0), Toast.LENGTH_LONG).show();
    	}
    }
}