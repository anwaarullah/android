package com.sample.plot;

import java.util.Arrays;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;

import com.androidplot.series.XYSeries;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;

/**
 * The simplest possible example of using AndroidPlot to plot some data.
 */
public class AndroidPlotDemoActivity extends Activity
{

	private XYPlot mySimpleXYPlot;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		// initialize our XYPlot reference:
		mySimpleXYPlot = (XYPlot) findViewById(R.id.mySimpleXYPlot);

		// Create a couple arrays of y-values to plot:
		Number[] series1Numbers = {1.5, 5.4, 5, -2, 7, 4};
		Number[] series2Numbers = {-1, 0, 1, 0, -1, 0, 1, 0, -1, 0};

		// Turn the above arrays into XYSeries':
		XYSeries series1 = new SimpleXYSeries(
				Arrays.asList(series1Numbers),          // SimpleXYSeries takes a List so turn our array into a List
				SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, // Y_VALS_ONLY means use the element index as the x value
		"Sine Wave");                             // Set the display title of the series

		// same as above
		XYSeries series2 = new SimpleXYSeries(Arrays.asList(series2Numbers), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Square Wave");

		// Create a formatter to use for drawing a series using LineAndPointRenderer:
		LineAndPointFormatter series1Format = new LineAndPointFormatter(
				Color.RED,                   // line color
				Color.GREEN,                   // point color
				null);                                  // fill color (none)

		// add a new series' to the xyplot:
		mySimpleXYPlot.addSeries(series1, series1Format);

		// same as above:
		mySimpleXYPlot.addSeries(series2,
				new LineAndPointFormatter(Color.BLUE, Color.YELLOW, null));

		// reduce the number of range labels
		mySimpleXYPlot.setTicksPerRangeLabel(1);

		// by default, AndroidPlot displays developer guides to aid in laying out your plot.
		// To get rid of them call disableAllMarkup():
		mySimpleXYPlot.disableAllMarkup();
	}
}